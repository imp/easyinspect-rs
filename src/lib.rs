use std::fmt;

pub trait Inspect
where
    Self: Sized,
{
    fn inspect(&self) -> &Self;
}


impl<T: fmt::Debug> Inspect for T {
    fn inspect(&self) -> &Self {
        let debug = |object: &T| println!("{:?}", object);
        debug(self);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn u8() {
        let item = 123u8;
        let item2 = item.inspect() + 1;
        assert_eq!(item2, 124);
    }

    #[test]
    fn string() {
        let item = String::from("123");
        assert_eq!(item.inspect().len(), 3);
    }
}
